FROM registry.access.redhat.com/jboss-fuse-6/fis-java-openshift
EXPOSE 1000 8080 8081 8778
COPY target/api-vm-provider-1.0.0.jar  /var/jboss/
CMD  exec java -jar /var/jboss/api-vm-provider-1.0.0.jar
