# Spring-Boot Camel XML QuickStart

This example demonstrates how to configure Camel routes in Spring Boot via
a Spring XML configuration file.

The application utilizes the Spring [`@ImportResource`](http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/context/annotation/ImportResource.html) annotation to load a Camel Context definition via a [camel-context.xml](src/main/resources/spring/camel-context.xml) file on the classpath.

All commands below requires one of these:
- be logged in to the targeted OpenShift instance (using oc login command line tool for instance)
- configure properties to specify to which OpenShift instance it should connect

### Building

The example can be built with

    mvn clean package


### Running the example locally

The example can be run locally using the following Maven goal:

    mvn spring-boot:run


### Running the example on Kubernetes

It is assumed a running Kubernetes platform is already running. If not you can find details how to [get started](http://fabric8.io/guide/getStarted/index.html).

Assuming your current shell is connected to Kubernetes or OpenShift so that you can type a command like

```
kubectl get pods
```

or for OpenShift

```
oc get pods
```

Then the following command will package your app and run it on Kubernetes:

```
mvn fabric8:run
```

## Using the JenkinsPipeline

this pipeline is written to run in the master node. Since this image uses Java and Maven, these tools need to be provided by the environment. Jenkins already has a solution for this. Go to Manage Jenkins > Global tools Configurations:
  - add a jdk installation called java8 (java 8u172)
  - add a maven 3.5.3 installation

### Create pipeline
    oc create -f jenkinsfile.yml
